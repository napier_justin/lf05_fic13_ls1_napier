import java.util.*;
 
public class bmi {
  
  public static void main(String[] args) {
    
    Scanner einlesen = new Scanner(System.in);
    int wert, wert2; 
    String ausgabe = "";
  
    
    double gewicht;
    double kilogramm_bmi;
    double kilogramm; 
    String geschlecht;
    double bmi;
    boolean männlich;
    
    
    System.out.println("Bitte geben Sie Ihre Körpergröße in [cm] an:" );
    kilogramm = einlesen.nextDouble(); //Eingabe einer Körpergröße in cm
    System.out.print("Bitte geben Sie jetzt Ihr Gewicht in [kg] an:");
    gewicht = einlesen.nextDouble(); //Eingabe des gewichtes in Kilogramm
   
    System.out.print("Als letztes geben Sie bitte Ihr Geschlecht an [m/w]:");
    geschlecht = einlesen.next(); //Bestimmung ob geschlecht bei "m", oder weiblich bei "w"
    
    
    kilogramm_bmi = kilogramm/100;
    bmi = gewicht/(kilogramm_bmi*kilogramm_bmi);
    
    if (geschlecht.contains("m")){
      if (bmi<20){
        ausgabe="Untergewicht " + bmi;
      }
      else if(bmi>=20 && bmi<=25){
        ausgabe="Normalgewicht " + bmi;
      }
      else{
        ausgabe="Übergewicht " + bmi;
      }
    }
    else if (geschlecht.contains("w")){
      if (bmi<19){
        ausgabe="Sie sind übergewichtig  " + bmi;
      }
      else if(bmi>=19 && bmi<=24){
        ausgabe="Sie sind normalgewichtig " + bmi;
      }
      else{
        ausgabe="Sie haben Übergewicht " + bmi;
      }
    }
    else{
      ausgabe="Bitte gültiges Geschlecht wählen";
    }
    
    System.out.println("");
    System.out.println(ausgabe);    
    
    
  }
}
  
