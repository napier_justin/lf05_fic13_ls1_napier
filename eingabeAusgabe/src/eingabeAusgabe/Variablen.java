package eingabeAusgabe;

public class Variablen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
        Vereinbaren Sie eine geeignete Variable */
		
		int zaehler; 

  /* 2. Weisen Sie dem Zaehler den Wert 25 zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		zaehler= 25;
		System.out.println(zaehler);
		

  /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
        eines Programms ausgewaehlt werden.
        Vereinbaren Sie eine geeignete Variable */
		char punkt;

  /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
        und geben Sie ihn auf dem Bildschirm aus.*/
		punkt= 'C';
		System.out.println(punkt);
		

  /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
        notwendig.
        Vereinbaren Sie eine geeignete Variable */
		long genau;

  /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
        und geben Sie sie auf dem Bildschirm aus.*/
		genau=299792458;
		System.out.println(genau);
		

  /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
        soll die Anzahl der Mitglieder erfasst werden.
        Vereinbaren Sie eine geeignete Variable und initialisieren sie
        diese sinnvoll.*/
		byte mitgliederzahl;
		

  /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
		mitgliederzahl=7;
		System.out.println(mitgliederzahl);

  /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
        Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
        dem Bildschirm aus.*/
		double ladung;
		ladung= 1.602;
		System.out.println(ladung);

  /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
        Vereinbaren Sie eine geeignete Variable. */
        boolean zahlung;
  /*11. Die Zahlung ist erfolgt.
        Weisen Sie der Variable den entsprechenden Wert zu
        und geben Sie die Variable auf dem Bildschirm aus.*/
        zahlung= true;
        System.out.println(zahlung);
        System.out.println();
        
		
		
		
	}

}
